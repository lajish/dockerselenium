package com.docker.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.docker.utility.Utility;

public class Sample_Page {
	
	private WebDriver driver;
	
	public Sample_Page(WebDriver driver) {
		this.driver = driver;
	}
	
	By username = By.name("txtUsername");
	By password = By.name("txtPassword");
	By login = By.name("Submit");
	By AdminTab = By.xpath("//*[text()='PIM']/preceding::*[text()='Admin']/..");
	By AddUserButtton = By.xpath("//*[@id='btnAdd']");
	By SaveButton = By.xpath("//*[@id='btnSave']");
	
	public void login() {
		Utility.sendKeysAction(driver, username, "Admin");
		Utility.sendKeysAction(driver, password, "admin123");
		Utility.clickAction(driver, login);
	}
	
	public void naviagtionToAdmin() {
		Utility.clickAction(driver, AdminTab);
	}
	
	public void addUser() {
		Utility.clickAction(driver, AddUserButtton);
		Utility.clickAction(driver, SaveButton);
	}
}
