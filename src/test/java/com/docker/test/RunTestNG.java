package com.docker.test;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;

public class RunTestNG {

	public static void main(String[] args) {
		TestNG testng = new TestNG();
		List<String> suitefiles = new ArrayList<String>();
		suitefiles.add(".\\testng.xml");
		testng.setTestSuites(suitefiles);
		testng.run();
		
	}
}
