package com.docker.test;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.docker.page.Sample_Page;

public class Sample_Test extends BaseClass {
	//private WebDriver driver;
	private Sample_Page samplePage;
	

	@BeforeClass
	public void pageInitialization() {
		samplePage = new Sample_Page(driver);
	}

	@Test
	public void testOrangeCRMLiveDemo() throws Throwable {
		samplePage.login();
		System.out.println(" ");
		System.out.println("--->>> Login to application completed");
		samplePage.naviagtionToAdmin();
		System.out.println("--->>> Navigation to Admin section completed");
		samplePage.addUser();
		System.out.println("--->>> Test successfully executed");
		System.out.println(" ");
	}
}