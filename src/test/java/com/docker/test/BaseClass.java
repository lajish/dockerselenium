package com.docker.test;

import java.io.IOException;
import java.lang.reflect.Method;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.docker.factory.BrowserFactory;
import com.docker.library.WaitLibrary;
import com.docker.testRailWithJenkins.APIException;
import com.docker.testRailWithJenkins.TestRailIntegration;
import com.docker.utility.Helper;

public class BaseClass {

	protected WebDriver driver;
	private ExtentHtmlReporter htmlReporter;
	protected ExtentReports extent;
	protected TestRailIntegration oTestRailIntegration;
	protected ExtentTest logger;
	private int TEST_STATUS;

	@BeforeTest
	@Parameters({ "useDocker", "browser", "url", "runAccessibility", "useBrowserStack", "useChromeMobileEmulation",
			"browserStackOS", "browserStackOSVersion", "browserStackBrowserVersion", "chromeEmulationDeviceName" })
	public void initializeTest(Boolean useDocker, String browser, String url, boolean runAccessibility,
			boolean useBrowserStack, boolean useChromeMobileEmulation, String browserStackOS,
			String browserStackOSVersion, String browserStackBrowserVersion, String chromeEmulationDeviceName) {
		extentReportSetup();
		if (runAccessibility) {
			accessibilityReportSetup();
		}

		if (useBrowserStack) {
			System.out.println("-----Execution is running into BrowserStack-----");
			driver = BrowserFactory.getBrowserStackDesktop(browserStackOS, browserStackOSVersion, browser,
					browserStackBrowserVersion);
		}
		if (useChromeMobileEmulation) {
			System.out.println("-----Execution is running on Chrome Mobile Emulation-----");
			driver = BrowserFactory.getChromeMobileEmulation(chromeEmulationDeviceName);
		}

		if (useDocker) {
			System.out.println("-----Execution is going to be running on docker-----");
			driver = BrowserFactory.getRemoteDriverOnDocker(browser);
		} else {
			System.out.println("-----Execution is running on Normal desktop-----");
			driver = BrowserFactory.getBrowser(browser);
		}
		driver.get(url);
	}

	/*
	 * 
	 * 
	 */
	@BeforeMethod
	@Parameters({ "runAccessibility" })
	public void testSetUp(Boolean runAccessibility, Method method) {
		logger = extent.createTest(method.getName());
		if (runAccessibility)
			System.out.println("");
	}

	/*
	 * 
	 * 
	 */
	@AfterMethod
	@Parameters({ "trRunId", "trCaseId" })
	public void tearDown(String trRunId, String trCaseId, ITestResult result, Method method) throws IOException {
		oTestRailIntegration = new TestRailIntegration();
		if (result.getStatus() == ITestResult.FAILURE) {
			System.out.println("-->>> TEST FAILURE");
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getName() + " - Test Case Failed", ExtentColor.RED));
			logger.log(Status.FAIL,
					MarkupHelper.createLabel(result.getThrowable() + " - Test Case Failed", ExtentColor.RED));
			try {
				logger.fail("Failuer screenshot",
						MediaEntityBuilder.createScreenCaptureFromPath(Helper.captureScreenshot(driver, "")).build());
			} catch (Exception e) {
				e.printStackTrace();
			}
			TEST_STATUS = 5;
		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.log(Status.SKIP,
					MarkupHelper.createLabel(result.getName() + " - Test Case Skipped", ExtentColor.YELLOW));
			TEST_STATUS = 4;
		} else {
			TEST_STATUS = 1;
		}
		try {
			oTestRailIntegration.addResultForCase(trRunId, trCaseId, TEST_STATUS);
		} catch (APIException e) {
			logger.log(Status.FAIL, MarkupHelper.createLabel(e.getMessage(), ExtentColor.RED));
		}
	}

	/*
	 * 
	 * 
	 */
	@AfterTest
	@Parameters({ "runAccessibility" })
	public void testFinalization(Boolean runAccessibility) {
		extentReportFinalization();
		BrowserFactory.closeBrowser(driver);
		if (runAccessibility) {
			accessibilityReportFinalization();
		}
	}

	/*
	 * Extent Report Setup
	 */
	public void extentReportSetup() {
		/*
		 * htmlReporter = new ExtentHtmlReporter( System.getProperty("user.dir") +
		 * "/Reports/" + this.getClass().getSimpleName() + ".html"); extent = new
		 * ExtentReports();
		 */
		System.out.println("--->>> "+System.getProperty("user.dir"));
		htmlReporter = new ExtentHtmlReporter("/home/DockerTest/Reports/" + this.getClass().getSimpleName() + ".html");
		extent = new ExtentReports();

		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("Host Machine", "Test Machine");
		extent.setSystemInfo("Environment", "Test Environment");
		extent.setSystemInfo("Created By", "Majid Inayat");

		htmlReporter.config().setDocumentTitle("Test Automation Report");
		htmlReporter.config().setReportName(getClass().getSimpleName().replaceAll("_", " "));
		htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
		htmlReporter.config().setTheme(Theme.STANDARD);
	}

	/*
	 * Accessibility Report Setup
	 */
	public void accessibilityReportSetup() {
		// AttestConfiguration.configure().testSuiteName("Digital
		// DotCom").outputDirectory("././Reports");
		// AttestConfiguration.configure().forAuditSuite(getClass().getResource("././resources/config/attest.json"));
	}

	/*
	 * Finishes writing off into extent report
	 */
	public void extentReportFinalization() {
		extent.flush();
	}

	/*
	 * Finishes writing off into accessibility report
	 */
	public void accessibilityReportFinalization() {
		Runtime rt = Runtime.getRuntime();
		try {
			rt.exec("././src/test/resources/attest-reporter-win Reports --dest Reports --format html+junit");
		} catch (IOException e) {
			e.printStackTrace();
		}
		WaitLibrary.sleep(25000);
	}
}
