package com.docker.factory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.aventstack.extentreports.ExtentTest;


public class BrowserFactory {

	static WebDriver driver;
	protected static ExtentTest _logger;
	public static final String USERNAME = "lajish";
	public static final String AUTOMATE_KEY = "tMy6poG1rzE9fLxm3QPW";
	public static final String URL = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";

	/**
	 * Returns WebDriver object as per given browser name
	 * 
	 * @param browser
	 * @return
	 */
	public static WebDriver getBrowser(String browser) {
		if (browser.equalsIgnoreCase("Firefox")) {
			//WebDriverManager.firefoxdriver().setup();
			System.setProperty("webdriver.gecko.driver", "/usr/bin/geckodriver");
			driver = new FirefoxDriver();
		} else if (browser.equalsIgnoreCase("Chrome")) {
			//System.setProperty("webdriver.chrome.driver", new BrowserFactory().getResourceFile("chromedriver.exe"));
			//WebDriverManager.chromedriver().setup();
			System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
			System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
			ChromeOptions opt = new ChromeOptions();
			opt.addArguments("--headless");
			opt.addArguments("--no-sandbox");
			/*opt.addArguments("--disable-infobars");
			opt.addArguments("--disable-geolocation");*/
			driver = new ChromeDriver(opt);
		} else if (browser.equalsIgnoreCase("ChromeIncognito")) {
			System.setProperty(ChromeDriverService.CHROME_DRIVER_SILENT_OUTPUT_PROPERTY, "true");
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--incognito");
			options.addArguments("--disable-infobars");
			options.addArguments("--disable-geolocation");
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			driver = new ChromeDriver(options);
		} 
		 else if (browser.equalsIgnoreCase("Firefox")) {
				
				driver = new FirefoxDriver();
			}
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		return driver;
	}

	public String getResourceFile(String fileNameWithExtension) {
		ClassLoader classLoader = getClass().getClassLoader();
		return new File(classLoader.getResource(fileNameWithExtension).getFile()).getAbsolutePath();
	}

	/**
	 * Will read in a file and then return the file in Bytes
	 * 
	 * @param fileNameWithExtension
	 * @return
	 * @throws IOException
	 */
	public byte[] getResourceFileBytes(String fileNameWithExtension) throws IOException {
		byte[] fileBytes = Files.readAllBytes(Paths.get(fileNameWithExtension));
		return fileBytes;
	}

	/**
	 * 
	 */
	public InputStream getResourceFileAPI(String fileNameWithExtension)
			throws URISyntaxException, FileNotFoundException {
		InputStream file = new FileInputStream(fileNameWithExtension);
		return file;
	}

	public static WebDriver getBrowserStackDesktop(String OS, String OS_Version, String browser,
			String browser_version) {

		if(OS.equalsIgnoreCase("OS_X")){
			OS="OS X";
		}
		if(OS_Version.equalsIgnoreCase("High_Sierra")){
			OS_Version = "High Sierra";
		}
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("browser", browser);
		caps.setCapability("browser_version", browser_version);
		caps.setCapability("os", OS);
		caps.setCapability("os_version", OS_Version);
		caps.setCapability("resolution", "1920x1080");
		caps.setCapability("build", "1.0");
		caps.setCapability("browserstack.local", "true");
		caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		caps.setCapability(CapabilityType.HAS_NATIVE_EVENTS, true);
		// caps.setCapability(CapabilityType.PROXY, false);
		caps.setCapability(CapabilityType.SUPPORTS_LOCATION_CONTEXT, true);

		// caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, true);
		// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		try {
			driver = new RemoteWebDriver(new URL(URL), caps);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		return driver;
	}

	public static WebDriver getBrowserStackDesktopLocal(String OS, String OS_Version, String browser, String browser_version) {

		/*
		 * Local browserStackLocal = new Local(); HashMap<String, String>
		 * browserStackLocalArgs = new HashMap<String, String>();
		 * browserStackLocalArgs.put("key", "tMy6poG1rzE9fLxm3QPW");
		 */

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("browser", browser);
		caps.setCapability("browser_version", browser_version);
		caps.setCapability("os", OS);
		caps.setCapability("os_version", OS_Version);
		caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		caps.setCapability(CapabilityType.HAS_NATIVE_EVENTS, true);
		caps.setCapability(CapabilityType.SUPPORTS_LOCATION_CONTEXT, true);
		caps.setCapability("browserstack.local", "true");
		caps.setCapability("build", "1.0");
		caps.setCapability("acceptSslCerts", "true");
		
		// caps.setCapability(CapabilityType.PROXY, false);
		// caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, true);
		// DesiredCapabilities capabilities = DesiredCapabilities.chrome();

		try {
			driver = new RemoteWebDriver(new URL(URL), caps);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		return driver;
	}

	public static WebDriver getBrowserStackMobile(String phoneType, String deviceName, String os_version) {

		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("browserName", phoneType);
		caps.setCapability("device", deviceName);
		// caps.setCapability("realMobile", "false");
		caps.setCapability("os_version", os_version);
		caps.setCapability("build", "1.0");
		caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		caps.setCapability(CapabilityType.HAS_NATIVE_EVENTS, true);
		// caps.setCapability(CapabilityType.PROXY, false);
		caps.setCapability(CapabilityType.SUPPORTS_LOCATION_CONTEXT, true);
		// caps.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, true);
		// DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		try {
			driver = new RemoteWebDriver(new URL(URL), caps);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		return driver;
	}

	/*
	 * 
	 * 
	 */
	public static WebDriver getChromeMobileEmulation(String deviceName) {
		if (deviceName.contains("_")) {
			deviceName = deviceName.replace("_", " ");
		}
		System.setProperty("webdriver.chrome.driver", new BrowserFactory().getResourceFile("chromedriver.exe"));
		ChromeOptions opt = new ChromeOptions();
		Map<String, String> chromeMobileEmulation = new HashMap<String, String>();

		if (deviceName.equalsIgnoreCase("Samsung Galaxy S10")) {
			opt.setExperimentalOption("mobileEmulation", customMobileDevice(deviceName));
		} else if (deviceName.equalsIgnoreCase("iPhone 11")) {
			opt.setExperimentalOption("mobileEmulation", customMobileDevice(deviceName));
		} else {
			chromeMobileEmulation.put("deviceName", deviceName);
			opt.setExperimentalOption("mobileEmulation", chromeMobileEmulation);
		}
		opt.addArguments("--disable-infobars");
		opt.addArguments("--disable-geolocation");
		driver = new ChromeDriver(opt);
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		return driver;
	}
	
	public static WebDriver getRemoteDriverOnDocker(String browser) {
		
		if(browser.equalsIgnoreCase("Chrome")) {
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--start-maximized");
			options.setCapability(ChromeOptions.CAPABILITY, options);
			options.setCapability("browserName", "chrome");
			options.setCapability("acceptSslCerts", "true");
			options.setCapability("javascriptEnabled", "true");
			DesiredCapabilities cap = DesiredCapabilities.chrome();
			options.merge(cap);
			cap.setBrowserName(BrowserType.CHROME);
			try {
				driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), options);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			
			if(browser.equalsIgnoreCase("Firefox")) {
				//WebDriverManager.firefoxdriver().setup();
				driver = new FirefoxDriver();
			}
		}
		/*
		 * else cap.setBrowserName(BrowserType.FIREFOX);
		 */
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40, TimeUnit.SECONDS);
		return driver;
	}

	public static void closeBrowser(WebDriver driver) {
		driver.quit();
		/*
		 * try { Runtime.getRuntime().exec("TASKKILL /F /IM chromedriver.exe");
		 * } catch (IOException e) { }
		 * 
		 * try { Runtime.getRuntime().exec("TASKKILL /F /IM geckodriver.exe"); }
		 * catch (IOException e) { }
		 */
	}

	private static Map<String, Object> customMobileDevice(String deviceName) {
		Map<String, Object> mobileEmulation = new HashMap<>();
		Map<String, Object> deviceMetrics = new HashMap<>();
		switch (deviceName) {
		case "Samsung Galaxy S10":
			deviceMetrics.put("width", 360);
			deviceMetrics.put("height", 760);
			deviceMetrics.put("pixelRatio", 3.0);

			mobileEmulation.put("deviceMetrics", deviceMetrics);
			mobileEmulation.put("userAgent",
					"Mozilla/5.0 (Linux; Android 9.0; en-us; Samsung Galaxy S10 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/77.0.3865.166 Mobile Safari/535.19");
			break;

		case "iPhone 11":
			deviceMetrics.put("width", 414);
			deviceMetrics.put("height", 896);
			deviceMetrics.put("pixelRatio", 4.0);

			mobileEmulation.put("deviceMetrics", deviceMetrics);
			mobileEmulation.put("userAgent",
					"Mozilla/5.0 (Linux; iOS 12.0; en-us; iPhone 11 Build/JOP40D) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/77.0.3865.166 Mobile Safari/535.19");
			break;
		}
		return mobileEmulation;

	}

	public static WebDriver getBrowserZAP() {
		System.setProperty("webdriver.chrome.driver", new BrowserFactory().getResourceFile("chromedriver.exe"));
		Proxy proxy1 = new Proxy();
		proxy1.setHttpProxy("localhost:8080");
		proxy1.setSslProxy("localhost:8080");
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability("proxy", proxy1);
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		options.addArguments("--ignore-certificate-errors");
		options.addArguments("--allow-running-insecure-content");
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		driver = new ChromeDriver(capabilities);
		return driver;
	}

	public static void setLogger(ExtentTest logger) {
		_logger = logger;
	}
}