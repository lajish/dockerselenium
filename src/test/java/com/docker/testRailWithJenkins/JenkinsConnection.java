 package com.docker.testRailWithJenkins;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Build;
import com.offbytwo.jenkins.model.Job;
import com.offbytwo.jenkins.model.JobWithDetails;

public class JenkinsConnection {
	
	public static final String JENKINS_USERNAME = "xxxxx";
    public static final String JENKINS_PASSWORD = "xxxx";
    public static final String JENKINS_URL = "https://xxxx.testrail.io/";
    JenkinsServer oJenkinsServer;
    JobWithDetails oJobWithDetails;
	static String lastBuildUrl;

	public JenkinsConnection(String jobName){
		
		String certificatesTrustStorePath = "C:/Program Files/Java/jre1.8.0_172/lib/security/cacerts";
		System.setProperty("javax.net.ssl.trustStore", certificatesTrustStorePath);
		oJenkinsServer = new JenkinsServer(URI.create(JENKINS_URL), JENKINS_USERNAME, JENKINS_PASSWORD);
		try {
			oJobWithDetails = oJenkinsServer.getJob(jobName);
		} catch (IOException e) 
		{
			System.out.println("Unable to get Jenkins Job Details");
		}
	}
	
	public void getAllJobs() throws MalformedURLException, IOException, APIException, URISyntaxException {
		
		Map<String, Job> jobs = oJenkinsServer.getJobs();
		System.out.println(jobs.size());
		for (String jobName: jobs.keySet()) {
            System.out.println(jobName);
        }
		
		oJobWithDetails = oJenkinsServer.getJob("DotComProdFeature_Login_Test");
        Build lastBuild = oJobWithDetails.getLastBuild();
        lastBuildUrl = lastBuild.getUrl();
        System.out.println("Last build of `" + "DotComProdFeature_Login_Test" + "` at " + lastBuild.getUrl());
        
        Build lastSuccessfulBuild = oJobWithDetails.getLastSuccessfulBuild();
        Build lastUnSuccessfulBuild = oJobWithDetails.getLastUnsuccessfulBuild();
        System.out.println("Successful build of `" + "DotComProdFeature_Login_Test" + "` at " + lastSuccessfulBuild.getUrl());
        System.out.println("Last Aborted build of `" + "DotComProdFeature_Login_Test" + "` at " + lastUnSuccessfulBuild.getUrl());
        
        System.out.println("Successful build of `" + "DotComProdFeature_Login_Test" + "` at " + lastSuccessfulBuild.getUrl());
        String flag = null;
        if(lastBuild.getNumber()==lastSuccessfulBuild.getNumber()){
        	flag = "PASS";
        }
        else if(lastBuild.getNumber()==lastUnSuccessfulBuild.getNumber()) {
        	flag = "FAIL";
        }
        System.out.println("Last Build status is "+flag); 
       
	}
       public void testRailPushResult(){
    	   
       }
	

}
