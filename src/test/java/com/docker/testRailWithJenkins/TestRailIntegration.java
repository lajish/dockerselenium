package com.docker.testRailWithJenkins;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

public class TestRailIntegration {
 
	public static String TEST_RUN_ID = "";
    public static final String TESTRAIL_USERNAME = "xxx@xxx.com";
    public static final String TESTRAIL_PASSWORD = "test123";
    public static final String RAILS_ENGINE_URL = "https://xxx.testrail.io/";
    public static final int TEST_CASE_PASSED_STATUS = 1;
    public static final int TEST_CASE_FAILED_STATUS = 5;
    public static APIClient client;
    
   /* 1	Passed
    2	Blocked
    3	Untested
    4	Retest
    5	Failed*/
    
    public TestRailIntegration() {
		client = new APIClient(RAILS_ENGINE_URL);
		client.setUser(TESTRAIL_USERNAME);
		client.setPassword(TESTRAIL_PASSWORD);
	}
	
	/*public void callTestRail() throws MalformedURLException, IOException, APIException{
		
		APIClieclient = createTestRailConnection();
		getCase(client, 21);
		addResultForCase(client);
		
	}*/

	public void addResultForCase(String runID, String caseID, int status) throws MalformedURLException, IOException, APIException {
		Map data = new HashMap();
        data.put("status_id", status);
        data.put("comment", "Test Executed - Status updated automatically from test automation.");
        client.sendPost("add_result_for_case/"+runID+"/"+caseID,data );
	}

	public void getCase(APIClient client, int Case_ID) throws MalformedURLException, IOException, APIException {
		JSONObject c = (JSONObject) client.sendGet("get_case/"+Case_ID);
		//System.out.println(c.toString());
		//System.out.println(c.get("title"));
		System.out.println(c.get("custom_steps"));
	}

	
 
}
