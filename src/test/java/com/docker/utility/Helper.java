package com.docker.utility;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.ExtentTest;
import com.docker.library.WaitLibrary;

public class Helper {
	static ExtentTest logger;
	public static String captureScreenshot(WebDriver driver, String testName) {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File src = ts.getScreenshotAs(OutputType.FILE);
		String destination = "./Screenshots/" + new SimpleDateFormat("dd-MMM_HH-mm-SSS").format(new Date()) + ".png";
		try {
			FileUtils.copyFile(src, new File(destination));
		} catch (IOException e) {
			System.out.println("Failed to take screenshot " + e.getMessage());
		}
		return "https://jenkins.dcoe.tcfbank.com/job/" + testName + "/ws/" + destination;
	}

	public static void scrollPageToView(WebDriver driver, By locator) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",Utility.checkElementClick(driver, locator));
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
		}
	}

	/*
	 * public static void scrollPageToView(WebDriver driver, By locator, ExtentTest
	 * logger) { ((JavascriptExecutor)
	 * driver).executeScript("arguments[0].scrollIntoView(true);",
	 * Utility.checkElementClick(driver, locator, logger)); try {
	 * Thread.sleep(2000); } catch (InterruptedException e) {
	 * 
	 * } }
	 */

	public static void switchToNewTab(WebDriver driver) {

		WaitLibrary.sleep(5000);
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
	}

	public static void switchToPreviousTab(WebDriver driver) {
		WaitLibrary.sleep(5000);
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.close();
		driver.switchTo().window(tabs.get(0));
		WaitLibrary.sleep(2000);
	}

	public static String subStringExtraction(String actualString, int StartIndex, int lastIndex) {
		return actualString.substring(StartIndex, lastIndex);
	}
	
	public static String captureActualScreenshot(WebDriver driver, String testName) {
		TakesScreenshot ts = (TakesScreenshot) driver;
		File src = ts.getScreenshotAs(OutputType.FILE);
		String destination = "./Screenshots/" + new SimpleDateFormat("dd-MMM_HH-mm-SSS").format(new Date()) + ".png";
		try {
			FileUtils.copyFile(src, new File(destination));
		} catch (IOException e) {
			System.out.println("Failed to take screenshot " + e.getMessage());
		}
		return destination;
	}
	
	/**
	 * Will return the a random number
	 * @return
	 */
	public String getRandomNumber() {
		return String.valueOf(System.currentTimeMillis() % 10000);
	}

}
