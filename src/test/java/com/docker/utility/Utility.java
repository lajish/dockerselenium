package com.docker.utility;

import java.time.Duration;
import java.util.function.Function;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.docker.library.WaitLibrary;

public class Utility {

	public ExtentTest logger;
	
	public static WebElement checkElementClick(WebDriver driver, By locator) {
		WebElement element;
		Wait<WebDriver> wait = new FluentWait<WebDriver>(driver).withTimeout(Duration.ofSeconds(20))
				.pollingEvery(Duration.ofSeconds(1)).ignoring(NoSuchElementException.class);

		element = wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(locator);
			}
		});
		wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		return element;
	}
	
	public static void clickAction(WebDriver driver, By locator) {
		checkElementClick(driver, locator).click();
		WaitLibrary.sleep(1000);
	}
	
	public static void sendKeysAction(WebDriver driver, By locator, String stringToPass) {
		checkElementClick(driver, locator).sendKeys(stringToPass);
	}

	public static void callLogger(ExtentTest logger, String status, String messgae) {
		logger.log(Status.valueOf(status), messgae);
	}
	
	public static void clickUsingJS(WebDriver driver, By locator){
		JavascriptExecutor js = (JavascriptExecutor)driver; 
		js.executeScript("arguments[0].click();", driver.findElement(locator));
	}
}
